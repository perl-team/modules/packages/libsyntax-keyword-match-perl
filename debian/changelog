libsyntax-keyword-match-perl (0.15-1) unstable; urgency=medium

  * Import upstream version 0.15.

 -- gregor herrmann <gregoa@debian.org>  Wed, 24 Jul 2024 16:52:00 +0200

libsyntax-keyword-match-perl (0.14-1) unstable; urgency=medium

  * Import upstream version 0.14.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Thu, 09 May 2024 00:11:32 +0200

libsyntax-keyword-match-perl (0.13-2) unstable; urgency=medium

  * Remove generated files via debian/clean. (Closes: #1045362)

 -- gregor herrmann <gregoa@debian.org>  Sun, 13 Aug 2023 22:39:00 +0200

libsyntax-keyword-match-perl (0.13-1) unstable; urgency=medium

  * Import upstream version 0.13.
  * Update build and runtime dependencies.

 -- gregor herrmann <gregoa@debian.org>  Wed, 26 Jul 2023 17:15:43 +0200

libsyntax-keyword-match-perl (0.12-2) unstable; urgency=medium

  * Drop build dependency on libfuture-asyncawait-perl (and libfuture-
    perl) to break a dependency cycle.
    Thanks to Niko Tyni for the bug report. (Closes: #1040220)

 -- gregor herrmann <gregoa@debian.org>  Mon, 03 Jul 2023 18:23:15 +0200

libsyntax-keyword-match-perl (0.12-1) unstable; urgency=medium

  * Import upstream version 0.12.
  * Update years of upstream and packaging copyright.
  * Update test dependencies.

 -- gregor herrmann <gregoa@debian.org>  Wed, 21 Jun 2023 22:24:49 +0200

libsyntax-keyword-match-perl (0.10-1) unstable; urgency=medium

  * Import upstream version 0.10.
  * Declare compliance with Debian Policy 4.6.2.
  * Update Build-Depends for cross builds.

 -- gregor herrmann <gregoa@debian.org>  Mon, 26 Dec 2022 04:58:09 +0100

libsyntax-keyword-match-perl (0.09-1) unstable; urgency=medium

  * Import upstream version 0.09.
  * Add new test dependencies.
  * Update years of upstream and packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 25 Feb 2022 17:57:48 +0100

libsyntax-keyword-match-perl (0.08-2) unstable; urgency=medium

  * Fix typo in long description.
    Thanks to Jonas Smedegaard.

 -- gregor herrmann <gregoa@debian.org>  Wed, 29 Sep 2021 15:35:05 +0200

libsyntax-keyword-match-perl (0.08-1) unstable; urgency=medium

  * Initial release (closes: #994514).

 -- gregor herrmann <gregoa@debian.org>  Fri, 17 Sep 2021 02:21:45 +0200
